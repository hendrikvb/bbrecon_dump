# bbrecon_dump

Bug Bounty Recon Dumps

These are a number of recon dumps for bug bounty programs.

* Subdomains are generated using online sources, assetfinder, amass.
* Further filtering using httpx:

```
cat subdomains.txt | httpx -rate-limit 5 -o httpx.txt -threads 1 -status-code -title -content-length -location | tee httpx_verbose.txt
cat httpx.txt | sed 's/\x1b\[[0-9;]*m//g' | grep "[200]" | cut -d' ' -f1 > redbull_filtered.txt`
```

Running this output through gau and waybackurls will produce a few hundred megs of data.

On Red Bull (and most other programs), limit the number of requests through ffuf (tweak accordingly)
```
ffuf -rate=5 -u https://URL -w common.txt -fc 403 -t 1
```
